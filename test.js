// visual regression tests // screenshot maker 1.0 - experimental
// requirements: windows, NODE.js
// open command line and type: casperjs.bat test test.js 
// this will generate either full screenshots (if testing = false) or screenshots of particular selectors when testing = true.

var phantomcss = require("./../phantomcss.js");
var testdate = new Date();
    testdatetime = testdate.getFullYear() + pad(testdate.getMonth() +1) + pad(testdate.getDate());

phantomcss.init({
    screenshotRoot: './screenshots/'+testdatetime,
    failedComparisonsRoot: './screenshots/'+testdatetime+'/failures',
    comparisonResultRoot: './screenshots/'+testdatetime+'/results',
    addLabelToFailedImage: false
    // mismatchTolerance: 0.05
});

// true = developer testing, will use phantomcss, execute tests and provide comparison shots for given selector
// false = only shooting the screenshots
var testing = false;

// what DOM element are we testing
var selector = "#content";

var screenshotNow = new Date(),
    screenshotDateTime = screenshotNow.getFullYear() + pad(screenshotNow.getMonth() + 1) + pad(screenshotNow.getDate()) + '-' + pad(screenshotNow.getHours()) + pad(screenshotNow.getMinutes()) + pad(screenshotNow.getSeconds()),

    // root = "http://mysite.com/directory/"
    // urls = [
    //   root+"page1.html",
    //   root+"page2.html"
    // ]

    urls = [
        "http://www.google.com",
        "http://www.yahoo.com"
    ]

    viewports = [
      {
        'name': '[1]-smartphone-portrait',
        "echoname": "smartphone, portrait",
        'viewport': {width: 320, height: 480}
      },
      {
        'name': '[2]-smartphone-landscape',
        "echoname": "smartphone, landscape",
        'viewport': {width: 480, height: 320}
      },
      {
        'name': '[3]-tablet-portrait',
        "echoname": "tablet, portrait",
        'viewport': {width: 768, height: 1024}
      },
      {
        'name': '[4]-tablet-landscape',
        "echoname": "tablet, landscape",
        'viewport': {width: 1024, height: 768}
      },
      {
        'name': '[5]-laptop-standard',
        "echoname": "laptop",
        'viewport': {width: 1280, height: 1024}
      }
      // {
      //   'name': '[6]-desktop-standard',
      //   "echoname" : "desktop",
      //   'viewport': {width: 1600, height: 900}
      // }
    ];

casper.start();
var i = 0;

casper.each(viewports, function(casper, viewport){
    this.then(function(){
        this.viewport(viewport.viewport.width, viewport.viewport.height);
    })

    this.each(urls, function(self, url){
        this.thenOpen(url, function(){
            this.wait(500); // if you're testing on local webserver you might want to lower down this value but if the screenshots are lacking images (common problem when shooting larger areas) then dont lower this value too much.
            i++;
            this.then(function(){
                var geturl = this.getCurrentUrl();
                var thename = url.substring(geturl.lastIndexOf("/")+1).replace(".html","").replace("?tab=", "-");

                this.echo("Capturing: "+viewport.viewport.width+"x"+viewport.viewport.height+" ["+viewport.echoname+"] "+thename);
                if ( !testing ) {
                    this.capture("screenshots/" + screenshotDateTime + "/" + thename + "-" + viewport.name + "-" + viewport.viewport.width + "x" + viewport.viewport.height + ".png")
                }
                else {
                    phantomcss.screenshot(selector);
                }
                
            });
        })
    });

})

casper.then(function check_screenshots() {
  phantomcss.compareAll();
});

casper.then(function end_it(){
  casper.test.done();
});

casper.run(function(){
  console.log('\nTHE END.');
  phantom.exit(phantomcss.getExitStatus());
});



function pad(number) {
  var r = String(number);
  if ( r.length === 1 ) {
    r = '0' + r;
  }
  return r;
}
